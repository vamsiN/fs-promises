/*
    Folder structure:
        ├── problem1.js
        ├── problem2.js
        └── test
            ├── testProblem1.js
            └── testProblem2.js
*/

/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs');
const path = require('path')

const writeFilePromise = (file, data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, data, (error => {
            error ? reject(error) : resolve('file created with promise')
        }))
    })
}

const deleteFilePromise = (file) => {
    return new Promise((resolve, reject) => {
        fs.unlink(file, (error) => {
            error ? reject(error) : resolve(`${file} file is deleted with promise`)
        });
    })
}

function createAndDeleteRandomFiles() {

    fs.mkdir('randomJsonFiles', (error) => {
        if (error) {
            console.log(error)
        }
        console.log("new Directory created")
    })

    const fileNamesArray = ["file1.json", "file2.json"]

    fileNamesArray.forEach((fileName) => {
        writeFilePromise(path.join("randomJsonFiles", fileName), `im in ${fileName}`).then(() => {
            console.log(`${fileName} created`)
        })
    })

    fs.readdir("randomJsonFiles", (error, files) => {
        if (error) { console.log(error) }

        files.forEach((file) => {
            deleteFilePromise(path.join("randomJsonFiles", file))
                .then(() => {
                    console.log(`${file} deleted`)
                })

        })
    })


}

module.exports = createAndDeleteRandomFiles