const fs = require('fs');


const readFilePromise = (file) => {
    return new Promise((resolve, reject) => {
        fs.readFile(file, 'utf-8', (error, data) => {
            if (error) reject(error);
            //console.log(data)
            resolve(data)
        })
    })
}

const writeFilePromise = (file, data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, data, (error => {
            error ? reject(error) : resolve('file created with promise')
        }))
    })
}

const appendFilePromise = (file, data) => {
    return new Promise((resolve, reject) => {
        fs.appendFile(file, '\n' + data, (error => {
            error ? reject(error) : resolve('data appended with promise')
        }))
    })
}

const deleteFilePromise = (file) => {
    return new Promise((resolve, reject) => {
        fs.unlink(file, (error) => {
            error ? reject(error) : resolve(`${file} file is deleted with promise`)
        });
    })
}


function operationsOnLipsumFile() {


    readFilePromise('lipsum.txt')
        .then(data => {
            writeFilePromise("upperCaseContent.txt", data.toUpperCase()).then(() => { console.log("upperCaseContent.txt file created") })
            writeFilePromise('filenames.txt', 'upperCaseContent.txt').then(() => { console.log("upperCaseContent.txt file name stored in filenames.txt file") })
        })
        .catch(error => console.log(error))

    readFilePromise('upperCaseContent.txt')
        .then(data => {

            const lowerCaseSentencesArray = data.toLowerCase().split(".").map((each) => {
                return each.trim()
            }).filter((each) => {
                return each != ""
            })

            lowerCaseSentencesArray.forEach((each, index) => {
                if (index == 0) {
                    writeFilePromise("lowerCaseContent.txt", each)
                    //.then(() => { console.log(`first line adde to lowerCaseContent.txt file`) })
                } else {
                    appendFilePromise("lowerCaseContent.txt", each)
                    //.then(() => { console.log("sentence appended to lowerCaseContent.txt") })

                }
            })

            appendFilePromise('filenames.txt', 'lowerCaseContent.txt')
            //.then(() => { console.log("lowerCaseContent.txt file name stored in filenames.txt file") })

        }).catch((error) => console.log(error))


    readFilePromise('upperCaseContent.txt')
        .then(data => {
            const upperCaseSentencesArray = data.split('\n').filter((each) => each != "")
            upperCaseSentencesArray.forEach((each, index) => {
                if (index == 0) {
                    writeFilePromise('output.txt', each)
                } else {
                    appendFilePromise('output.txt', each)
                }

            })
        }).catch((error) => console.log(error))

    readFilePromise('lowerCaseContent.txt')
        .then(data => {
            const upperCaseSentencesArray = data.split('\n').filter((each) => each != "")
            upperCaseSentencesArray.forEach((each, index) => {
                appendFilePromise('output.txt', each)

            })
        }).catch((error) => console.log(error))

    readFilePromise('output.txt')
        .then(data => {
            const allSentencesArray = data.split('\n').filter((each) => each != "")
            allSentencesArray.sort()

            writeFilePromise('output.txt', allSentencesArray.join('\n'))
        }).catch((error) => { console.log(error) })

    writeFilePromise('filenames.txt', 'output.txt')
        .then(() => {
            console.log('output.txt file name stored in filenames.txt')
        }).catch((error) => { console.log(error) })

    readFilePromise('filenames.txt')
        .then(data => {
            const filenamesArray = data.split('\n').filter((each) => each != "")
            filenamesArray.forEach((each) => {
                deleteFilePromise(each)

            })
        }).catch((error) => console.log(error))





}

operationsOnLipsumFile()

module.exports = operationsOnLipsumFile




